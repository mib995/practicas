package es.unican.is2.banco;

@SuppressWarnings("serial")
public class DatoErroneoException extends RuntimeException {
	
	public DatoErroneoException (String mensaje) {
		super(mensaje);
	}

}
