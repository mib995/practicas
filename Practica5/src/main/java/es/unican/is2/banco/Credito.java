package es.unican.is2.banco;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Credito extends Tarjeta {
	
	private static final double COMISION = 0.05;
	private double credito;
	private List<Movimiento> movimientosMensuales;
	private List<Movimiento> historicoMovimientos;

	public Credito(String numero, String titular, double credito) {
		super(numero, titular);
		this.credito = credito;
		movimientosMensuales = new ArrayList<Movimiento>();
		historicoMovimientos = new ArrayList<Movimiento>();
		saldo = 0;
	}

	@Override
	public void ingresar(double x) throws DatoErroneoException{
		if (x <= 0)
			throw new DatoErroneoException("No se puede ingresar una cantidad negativa");
		
		Movimiento m = new Movimiento("Ingreso en cajero autom�tico", x);
		movimientosMensuales.add(m);
		saldo += x;
	}
		
	@Override
	public void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (x<0)
			throw new DatoErroneoException("No se puede retirar una cantidad negativa");
		
		x += x * COMISION;
		Movimiento m = new Movimiento("Retirada en cajero autom�tico", -x);
		
		if (x > getSaldo()+credito)
			throw new SaldoInsuficienteException("Cr�dito insuficiente");
		else {
			movimientosMensuales.add(m);
			saldo -= x;
		}
	}

	@Override
	public void pagoEnEstablecimiento(String datos, double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (x<0)
			throw new DatoErroneoException("No se puede retirar una cantidad negativa");
		if (x > getSaldo()+credito)
			throw new SaldoInsuficienteException("Saldo insuficiente");
		
		Movimiento m = new Movimiento("Compra a cr�dito en: " + datos, -x);
		movimientosMensuales.add(m);
		saldo = saldo -x;
	}
	
	@Override
	public double getSaldo() {
		return saldo;
	}
	
	
	/**
	 * M�todo que se invoca autom�ticamente el d�a 1 de cada mes
	 */
	public void liquidar() {
		Movimiento liq = new Movimiento();
		liq.setConcepto("Liquidaci�n de operaciones tarj. cr�dito");
		double r = 0.0;
		for (int i = 0; i < this.movimientosMensuales.size(); i++) {
			Movimiento m = (Movimiento) movimientosMensuales.get(i);
			r += m.getImporte();
		}
		liq.setImporte(r);

		if (r != 0)
			super.getCuentaAsociada().addMovimiento(liq);
		historicoMovimientos.addAll(movimientosMensuales);
		movimientosMensuales.clear();
		this.saldo = 0;
	}
	
	public Date getCaducidad() {
		return super.getCuentaAsociada().getCaducidadCredito();
	}

	public List<Movimiento> getMovimientosUltimoMes() {
		return movimientosMensuales;
	}
	
	public List<Movimiento> getMovimientos() {
		return movimientosMensuales;
	}

}