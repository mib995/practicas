package es.unican.is2.banco;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;
public class Cuenta {

	private String numeroCuenta;
	private Cliente cliente;
	private List<Movimiento> movimientos;
	private Date fechaDeCaducidadTarjetaDebito;
	private Date fechaDeCaducidadTarjetaCredito;
	private double limiteDebito;
	private double saldo=0;
	
	public Cuenta(String numeroCuenta, String mTitular, String mDireccion,
			String mtelefono, String mDNI, Date FechaDeCaducidadTarjetaDebito, Date FechaDeCaducidadTarjetaCredito) {

		this.numeroCuenta = numeroCuenta;
		cliente = new Cliente(mTitular, mDireccion, mtelefono, mDNI);
		this.fechaDeCaducidadTarjetaDebito=FechaDeCaducidadTarjetaDebito;
		this.fechaDeCaducidadTarjetaCredito=FechaDeCaducidadTarjetaCredito;
		movimientos=new LinkedList<Movimiento>();
		limiteDebito = 1000;
	}
	
	public Cuenta(String numeroCuenta, Cliente titular, Date FechaDeCaducidadTarjetaDebito, Date FechaDeCaducidadTarjetaCredito) {

		this.numeroCuenta = numeroCuenta;
		cliente=titular;
		this.fechaDeCaducidadTarjetaDebito=FechaDeCaducidadTarjetaDebito;
		this.fechaDeCaducidadTarjetaCredito=FechaDeCaducidadTarjetaCredito;
		movimientos=new LinkedList<Movimiento>();
		limiteDebito = 1000;
	}


	public Movimiento ingresar(double x) throws DatoErroneoException {
		return ingresar("Ingreso en efectivo", x);
	}

	public Movimiento retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		return retirar("Retirada de efectivo", x);
	}

	
	public Movimiento ingresar(String concepto, double x) throws DatoErroneoException {
		if (x <= 0)
			throw new DatoErroneoException("No se puede ingresar una cantidad negativa");
		Movimiento m = new Movimiento(concepto, x);
		this.addMovimiento(m);
		return m;
	}

	public Movimiento retirar(String concepto, double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (getSaldo() < x)
			throw new SaldoInsuficienteException("Saldo insuficiente");
		if (x <= 0)
			throw new DatoErroneoException("No se puede retirar una cantidad negativa");
		Movimiento m = new Movimiento(concepto, -x);
		this.addMovimiento(m);
		return m;
	}

	
	public double getSaldo() {
		return this.saldo;
	}

	public void addMovimiento(Movimiento m) {
		this.saldo += m.getImporte();
		movimientos.add(m);
	}

	public List<Movimiento> getMovimientos() {
		return movimientos;
	}
	
	public Date getCaducidadDebito(){
		return this.fechaDeCaducidadTarjetaDebito;
	}
	
	public Date getCaducidadCredito(){
		return this.fechaDeCaducidadTarjetaCredito;
	}

	public double getLimiteDebito() {
		return limiteDebito;
	}

	public Cliente getCliente() {
		return cliente;
	}
	
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	
}