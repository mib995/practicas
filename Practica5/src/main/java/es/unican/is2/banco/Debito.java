package es.unican.is2.banco;

import java.util.Date;

public class Debito extends Tarjeta {
	
	public Debito(String numero, String titular, Date fechaCaducidad) {
		super(numero, titular);
	}
	
	@Override
	public void ingresar(double x) throws DatoErroneoException{
		super.getCuentaAsociada().ingresar("Ingreso en cajero autom�tico", x);
		saldo+=x;
	}

	@Override
	public void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		super.getCuentaAsociada().retirar("Retirada en cajero autom�tico", x);
		saldo-=x;
	}
	
	@Override
	public void pagoEnEstablecimiento(String datos, double x) throws SaldoInsuficienteException, DatoErroneoException {
		super.getCuentaAsociada().retirar("Compra en : " + datos, x);
		saldo-=x;
	}

	/**
	 * M�todo invocado autom�ticamente a las 00:00 de cada d�a
	 */
	@Override
	public void liquidar() {
		saldo = super.getCuentaAsociada().getLimiteDebito();
	}

}