package es.unican.is2.banco;

public abstract class Tarjeta {
	protected String numero, titular;
		
	protected Cuenta CuentaAsociada;

	protected double saldo;

	public Tarjeta(String numero, String titular) {
		this.numero = numero;
		this.titular = titular;
	}

	public void setCuenta(Cuenta c) {
		CuentaAsociada = c;
	}

	public abstract void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException;

	public abstract void pagoEnEstablecimiento(String datos, double x)
			throws SaldoInsuficienteException, DatoErroneoException;
	
	public abstract void ingresar(double x) throws DatoErroneoException;

	public double getSaldo() {
		return saldo;
	}

	public Cuenta getCuentaAsociada() {
		return CuentaAsociada;
	}

	public abstract void liquidar();
}