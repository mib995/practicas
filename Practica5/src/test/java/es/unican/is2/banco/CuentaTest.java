package es.unican.is2.banco;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

public class CuentaTest {
	static Cuenta cuenta;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cuenta = new Cuenta("794311", "Juan Gomez", "Torrelavega", "942589674","874214563X", new GregorianCalendar(2015, Calendar.DECEMBER, 12).getTime(), new GregorianCalendar(2015, Calendar.DECEMBER, 12).getTime());
	}// setUpBeforeClass

	@Test
	public void testGetSaldo() {

		assertTrue(cuenta.getSaldo() == 0);

		Movimiento movimientoa = new Movimiento();
		movimientoa.setImporte(100);

		cuenta.addMovimiento(movimientoa);
		assertTrue(cuenta.getSaldo() == 100);

		try {
			cuenta.retirar(50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getConcepto()
				.equals("Retirada de efectivo"));
		assertTrue(cuenta.getSaldo() == 50);

		try {
			cuenta.ingresar(-1);
		} catch (Exception e) {
			 //e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50);

		try {
			cuenta.ingresar(0.01);
		} catch (Exception e) {
			 //e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getConcepto()
				.equals("Ingreso en efectivo"));
		assertTrue(cuenta.getSaldo() == 50.01);

		try {
			cuenta.ingresar("R1", 0.5);
		} catch (Exception e) {
			 //e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50.51);

		try {
			cuenta.retirar("R1", 0.5);
		} catch (Exception e) {
			 //e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50.01);
		try {
			cuenta.retirar(-1);
		} catch (Exception e) {
			 //e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50.01);

		try {
			cuenta.retirar(0.01);
		} catch (Exception e) {
			 //e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50);
	}

	@Test
	public void testIngresar() {
		try {
			cuenta.ingresar(-20);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals(
					"No se puede ingresar una cantidad negativa"));
		}
	}

	@Test
	public void testRetirar() {
		try {
			cuenta.retirar(-20);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals(
					"No se puede retirar una cantidad negativa"));
		}
		
		try {
			cuenta.retirar(500);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals("Saldo insuficiente"));
		}
	}
}// EscenarioATest
