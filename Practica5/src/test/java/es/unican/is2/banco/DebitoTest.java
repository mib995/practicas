package es.unican.is2.banco;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.BeforeClass;
import org.junit.Test;


public class DebitoTest {
	static Tarjeta tDebito;
	static Cuenta cuenta;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cuenta = new Cuenta("794311", "Juan Gomez","Torrelavega", "942589674","874214563X",new GregorianCalendar(2015, Calendar.DECEMBER, 12).getTime(), new GregorianCalendar(2015, Calendar.DECEMBER, 12).getTime());
	}

	@Test
	public void testGetSaldo() {

		tDebito = new Debito("5468416521", "Marcos Garc�a", new Date());
		tDebito.setCuenta(cuenta);

		// ingresamos
		try {
			tDebito.ingresar(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getConcepto()
				.equals("Ingreso en cajero autom�tico"));
		assertTrue(tDebito.getSaldo() == 100);

		// retiramos
		try {
			tDebito.retirar(50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getConcepto()
				.equals("Retirada en cajero autom�tico"));
		assertTrue(tDebito.getSaldo() == 50);

		// pago desde establecimiento
	}
	@Test
	public void testPagoEnEstablecimiento() {
		try {
			tDebito.pagoEnEstablecimiento("Prueba1", 50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getConcepto()
				.equals("Compra en : Prueba1"));
		assertTrue(tDebito.getSaldo() == 0);

	}

	@Test
	public void testIngresar() {
		try {
			cuenta.ingresar(-20);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals(
					"No se puede ingresar una cantidad negativa"));
		}
	}
}// EscenarioBTest
