package es.unican.is2.practica3;

/**
 * Clase que representa un programa de lavado del lavavajillas
 * @author Mario
 *
 */
public class ProgramaLavado {

	private String nombre;
	private int tiempo;
	
	/**
	 * Constructor que crea un programa de lavado
	 * @param nombre nombre del programa de lavado
	 * @param tiempo tiempo que tarda en realizarse el lavado
	 */
	public ProgramaLavado(String nombre, int tiempo) {
		this.nombre = nombre;
		this.tiempo = tiempo;
	}

	/**
	 * Método que retorna el nombre del programa
	 * @return retorna el nombre del progama
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Método que retorna el tiempo de lavado de programa
	 * @return retorna el tiempo de lavado de programa
	 */
	public int getTiempo() {
		return tiempo;
	}

}
