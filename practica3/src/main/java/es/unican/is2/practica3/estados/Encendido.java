package es.unican.is2.practica3.estados;

import es.unican.is2.practica3.Lavavajillas;
import es.unican.is2.practica3.ProgramaLavado;

/**
 * Clase que representa el estado encendido del lavavajillas
 * @author Mario
 *
 */
public class Encendido extends LavavajillasState{

	/**
	 * Constructor que crea el estado
	 */
	public Encendido() {}

	@Override
	public void entryAction(Lavavajillas ctx) {}

	@Override
	public void exitAction(Lavavajillas ctx) {}

	@Override
	public void doAction(Lavavajillas ctx) {}

	@Override
	public void OnOff(Lavavajillas ctx) {
		this.exitAction(ctx);
		Apagado e = LavavajillasState.getApagado();
		ctx.setState(e);
		e.entryAction(ctx);
		e.doAction(ctx);
	}

	@Override
	public void Arrancar(Lavavajillas ctx) {
		ctx.setArrancar(!ctx.isArrancar());
	}

	@Override
	public void ProgramaSeleccionado(String nombre, Lavavajillas ctx) {
		ProgramaLavado aux = ctx.buscarPrograma(nombre);
		ctx.apagarLuzPrograma(ctx.getProgramaElegido().getNombre());
		ctx.encenderLuzPrograma(nombre);
		ctx.setProgramaElegido(aux);
		ctx.setTiempoFinLavado(aux.getTiempo());
	}

	@Override
	public void PuertaAbierta(Lavavajillas ctx) {}

	@Override
	public void PuertaCerrada(Lavavajillas ctx) {
		if(ctx.isArrancar()){
			this.exitAction(ctx);
			Lavando la = LavavajillasState.getLavando();
			ctx.setState(la);
			la.entryAction(ctx);
			la.doAction(ctx);
		}
	}

}
