package es.unican.is2.practica3;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
/**
 *
 *	Clase que sirve de interfaz para el abrir y cerrar de las puertas
 * @author Mario
 */
public class Puerta extends JFrame {
	
	//panel central
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Puerta(final Lavavajillas lavavajillas) {
		setTitle("Puerta");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 380, 159);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 2, 40, 40));
		
		//boton de puerta abierta
		JButton btnAbierta = new JButton("Puerta abierta");
		btnAbierta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lavavajillas.puertaAbierta();
			}
		});
		contentPane.add(btnAbierta);
		
		//boton de cerrar puerta
		JButton btnCerrada = new JButton("Puerta Cerrada");
		contentPane.add(btnCerrada);
		btnCerrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lavavajillas.puertaCerrada();
			}
		});
	}

}
