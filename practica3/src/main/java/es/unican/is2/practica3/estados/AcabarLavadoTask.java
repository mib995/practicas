package es.unican.is2.practica3.estados;

import java.util.TimerTask;

import es.unican.is2.practica3.Lavavajillas;

/**
 * Clase que se representa las acciones ha realizar cuando acabado el tiempo de lavado
 * @author Mario
 *
 */
public class AcabarLavadoTask extends TimerTask {
	private Lavavajillas context;
	
	/**
	 * Constructor que crea la tarea ha realizar cuando acaba el tiempo de lavado
	 * @param la objeto de lavavajillas para el contecto de la tarea
	 */
	public AcabarLavadoTask(Lavavajillas la) {
		context = la;
	} 
	
	/**
	 * Método que se realiza cuando acaba el tiempo del timer de acabar el tiempo de lavado
	 */
	public void run() { 
		LavavajillasState.getLavando().exitAction(context);
		context.setState(LavavajillasState.getApagado());  
		LavavajillasState.getApagado().entryAction(context); 
		LavavajillasState.getApagado().doAction(context);
	}
}
