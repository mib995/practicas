package es.unican.is2.practica3;

import java.util.HashMap;
import java.util.Map;

import es.unican.is2.practica3.estados.*;

/**
 * Clase que representa un lavavajillas empotrado
 * @author Mario
 *
 */
public class Lavavajillas {
	
	private boolean arrancar = false;
	private long tiempoFinLavado = 0;
	private LavavajillasState state; //estado
	private ProgramaLavado programaElegido;
	private long tiempoPausa = 0;
	
	private Map<String, ControladorLuz> lucesPrograma = new HashMap<String, ControladorLuz>(); //luces de la interfaz
	private Map<String, ProgramaLavado> programas = new HashMap<String, ProgramaLavado>(); //programas de lavado
	
	/**
	 * Constructor que crea el lavavajillas con sus programas y estados
	 */
	public Lavavajillas() {
		programas.put("ECO", new ProgramaLavado("ECO",1));
		programas.put("RAPIDO",new ProgramaLavado("RAPIDO",2));
		programas.put("INTENSO",new ProgramaLavado("INTENSO",20));
		programas.put("PRELAVADO",new ProgramaLavado("PRELAVADO",4));
		
		state = LavavajillasState.init(this);
	}
	
	/**
	 * Método que indica el incio del lavado
	 */
	public void start(){
		System.out.println("Lavavajillas encendido");
	}
	
	/**
	 * Método que indica el final del lavado
	 */
	public void stop(){
		System.out.println("Lavavajillas apagado");
	}
	
	/**
	 * Método que cambia el estado del lavavajillas
	 * @param state estado nuevo del lavavajillas
	 */
	public void setState(LavavajillasState state){
		this.state = state;
	}

	/**
	 * Método que trata la señal ON/OFF
	 */
	public void onOff(){
		state.OnOff(this);
	}

	/**
	 * Método que trata la señal de arrancar
	 */
	public void arrancar(){
		state.Arrancar(this);
	}

	/**
	 * Método que trata la señal ProgramaSeleccionado
	 * @param nombre nombre del programa selecionado
	 */
	public void ProgramaSeleccionado(String nombre){
		state.ProgramaSeleccionado(nombre,this);
	}

	/**
	 * Método que trata la señal de PuertaAbierta
	 */
	public void puertaAbierta(){
		state.PuertaAbierta(this);
	}
	
	/**
	 * Método que trata la señal de PuertaCerrada
	 */
	public void puertaCerrada(){
		state.PuertaCerrada(this);
	}

	/**
	 * Método que cambia el programa elegido para el lavavajillas
	 * @param programaElegido nuevo programa elegido
	 */
	public void setProgramaElegido(ProgramaLavado programaElegido) {
		this.programaElegido = programaElegido;
	}
	
	/**
	 * Método que busca el programa por el nombre
	 * @param nombre nombre del programa a buscar
	 * @return programa escogido
	 */
	public ProgramaLavado buscarPrograma(String nombre){
		return programas.get(nombre);
	}

	/**
	 * Método que busca la luz del programa por el nombre
	 * @param nombre nombre de la luz a buscar
	 * @return el controlador de la luz
	 */
	public ControladorLuz buscarLucesPrograma(String nombre) {
		return lucesPrograma.get(nombre);
	}
	
	/**
	 * Método que indica el tiempo de lavado restante
	 * @return tiempo de lavado restante
	 */
	public long getTiempoFinLavado() {
		return tiempoFinLavado;
	}

	/**
	 * Método que define el tiempo de fin de lavado
	 * @param tiempoFinLavado tiempo del fin del lavado
	 */
	public void setTiempoFinLavado(long tiempoFinLavado) {
		this.tiempoFinLavado = tiempoFinLavado;
	}

	/**
	 * Método que indica si el lavavajillas está arrancado
	 * @return true si el lavavajillas esta arrancado y false si no lo está
	 */
	public boolean isArrancar() {
		return arrancar;
	}
	
	/**
	 * Método que retorna el programa elegido para el lavado
	 * @return el programa elegido para el lavado
	 */
	public ProgramaLavado getProgramaElegido() {
		return programaElegido;
	}
	
	/**
	 * Método que cambia si el lavavajillas está arrancado o no
	 * @param arrancar true si se quiere arrancar y false en caso contrario
	 */
	public void setArrancar(boolean arrancar) {
		this.arrancar = arrancar;
	}

	/**
	 * Método que permite apagar todas las luces del lavavajillas
	 */
	public void apagarLuces(){
		for(ControladorLuz c : lucesPrograma.values())
			c.off();
	}

	/**
	 * Método que indica el tiempo que ha estado pausado el lavavajillas
	 * @return tiempo de pausado del lavavajillas
	 */
	public long getTiempoPausa() {
		return tiempoPausa;
	}

	/**
	 * Método que permite calcular el último tiempo de pausado
	 * @param tiempoPausa tiempo que el lavavajillas ha estado en pausa
	 */
	public void setTiempoPausa(long tiempoPausa) {
		this.tiempoPausa = tiempoPausa;
	}
	
	/**
	 * Método que permite anhadir un nuevo controlador de luces
	 * @param c controlador de luz nuevo
	 */
	public void anhadirContolador(ControladorLuz c){
		lucesPrograma.put(c.getNombre(), c);
	}

	/**
	 * Método que permite encender una luz de la interfaz
	 * @param nombre nombre de la luz a encender
	 */
	public void encenderLuzPrograma(String nombre){
		buscarLucesPrograma(nombre).on();
	}
	
	/**
	 * Método que permite apagar una luz de la interfaz
	 * @param nombre nombre de la luz a apagar
	 */
	public void apagarLuzPrograma(String nombre){
		buscarLucesPrograma(nombre).off();
	}
}
