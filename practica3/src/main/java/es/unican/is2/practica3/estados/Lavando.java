package es.unican.is2.practica3.estados;

import java.util.Timer;

import es.unican.is2.practica3.Lavavajillas;

/**
 * Clase que representa el estado lavando del lavavajillas
 * @author Mario
 *
 */
public class Lavando extends LavavajillasState {

	
	private Timer timer; 			//timer que indica el final del lavado
	private AcabarLavadoTask acabar;//tarea cuando finaliza el lavado
	
	/**
	 * Constructor que crea el estado
	 */
	public Lavando() {}

	@Override
	public void entryAction(Lavavajillas ctx) {
		ctx.start();
		timer = new Timer();
		acabar = new AcabarLavadoTask(ctx);
		ctx.setTiempoFinLavado((ctx.getTiempoFinLavado()*60*1000+ctx.getTiempoPausa())/60000);
		timer.schedule(acabar, ctx.getTiempoFinLavado()*60000);
	}

	@Override
	public void exitAction(Lavavajillas ctx) {
		ctx.setTiempoPausa(0);
		ctx.stop();	
	}

	@Override
	public void doAction(Lavavajillas ctx) {}

	@Override
	public void OnOff(Lavavajillas ctx) {}

	@Override
	public void Arrancar(Lavavajillas ctx) {}

	@Override
	public void ProgramaSeleccionado(String nombre, Lavavajillas ctx) {}

	@Override
	public void PuertaAbierta(Lavavajillas ctx) {
		timer.cancel();
		acabar.cancel();
		this.exitAction(ctx);
		Pausa pa = LavavajillasState.getPausa();
		ctx.setState(pa);
		pa.entryAction(ctx);
		pa.doAction(ctx);
		
	}

	@Override
	public void PuertaCerrada(Lavavajillas ctx) {}
	
}
