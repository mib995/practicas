package es.unican.is2.practica3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 *	Clase principal que sirve para crear la interfaz gráfica principal 
 *	y la inicialización del lavavajilals
 * @author Mario
 */
public class App{

	//interfaz del lavavajillas
	private JFrame frmLavavajillas;
	//lavavajillas
	private Lavavajillas lavavajillas;
	//interfaz de la puerta
	private Puerta puerta;
	//botones y leds principales
	private JButton btnEco;
	private JButton btnRapido;
	private JButton btnPrela;
	private JButton btnIntenso;
	private JButton btnOnoff;
	private JButton ledEco = new JButton("");
	private JButton ledRapido = new JButton("");
	private JButton ledON = new JButton("");
	private JButton ledPrela = new JButton("");
	private JButton ledIntenso = new JButton("");
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					 App window = new App();
					 window.frmLavavajillas.setVisible(true);
					 window.puerta.setVisible(true);
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		lavavajillas = new Lavavajillas();
		puerta = new Puerta(lavavajillas);
		lavavajillas.anhadirContolador(new ControladorLuz("ECO", ledEco));
		lavavajillas.anhadirContolador(new ControladorLuz("RAPIDO", ledRapido));
		lavavajillas.anhadirContolador(new ControladorLuz("INTENSO", ledIntenso));
		lavavajillas.anhadirContolador(new ControladorLuz("PRELAVADO", ledPrela));
		lavavajillas.anhadirContolador(new ControladorLuz("On/Off", ledON));
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmLavavajillas = new JFrame();
		frmLavavajillas.setTitle("Lavavajillas");
		frmLavavajillas.setBounds(100, 100, 550, 300);
		frmLavavajillas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLavavajillas.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panelCentral = new JPanel();
		frmLavavajillas.getContentPane().add(panelCentral, BorderLayout.WEST);
		panelCentral.setLayout(new GridLayout(4, 2, 10, 10));
		
		JPanel panelEco = new JPanel();
		panelCentral.add(panelEco);
		
		
		ledEco.setBackground(new Color(169, 169, 169));
		GroupLayout gl_panel_1 = new GroupLayout(panelEco);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(38)
					.addComponent(ledEco, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(40, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
					.addContainerGap(17, Short.MAX_VALUE)
					.addComponent(ledEco, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		panelEco.setLayout(gl_panel_1);
		
		btnEco = new JButton("Eco");
		btnEco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lavavajillas.ProgramaSeleccionado("ECO");
			}
		});
		panelCentral.add(btnEco);
		
		JPanel panelRapido = new JPanel();
		panelCentral.add(panelRapido);
		
		ledRapido.setBackground(new Color(169, 169, 169));
		GroupLayout gl_panel_3 = new GroupLayout(panelRapido);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGap(0, 109, Short.MAX_VALUE)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addGap(38)
					.addComponent(ledRapido, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(40, Short.MAX_VALUE))
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGap(0, 55, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_panel_3.createSequentialGroup()
					.addContainerGap(17, Short.MAX_VALUE)
					.addComponent(ledRapido, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		panelRapido.setLayout(gl_panel_3);
		
		btnRapido = new JButton("Rapido");
		panelCentral.add(btnRapido);
		btnRapido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lavavajillas.ProgramaSeleccionado("RAPIDO");
			}
		});
		
		
		JPanel panelPrela = new JPanel();
		panelCentral.add(panelPrela);
		
		
		ledPrela.setBackground(new Color(169, 169, 169));
		GroupLayout gl_panel_2 = new GroupLayout(panelPrela);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 109, Short.MAX_VALUE)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(38)
					.addComponent(ledPrela, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(40, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 55, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_panel_2.createSequentialGroup()
					.addContainerGap(17, Short.MAX_VALUE)
					.addComponent(ledPrela, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		panelPrela.setLayout(gl_panel_2);
		
		btnPrela = new JButton("Prelavado");
		panelCentral.add(btnPrela);
		btnPrela.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lavavajillas.ProgramaSeleccionado("PRELAVADO");
			}
		});
		
		JPanel panelIntenso = new JPanel();
		panelCentral.add(panelIntenso);
		
		
		ledIntenso.setBackground(new Color(169, 169, 169));
		GroupLayout gl_panel_4 = new GroupLayout(panelIntenso);
		gl_panel_4.setHorizontalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGap(0, 109, Short.MAX_VALUE)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addGap(38)
					.addComponent(ledIntenso, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(40, Short.MAX_VALUE))
		);
		gl_panel_4.setVerticalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGap(0, 55, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_panel_4.createSequentialGroup()
					.addContainerGap(17, Short.MAX_VALUE)
					.addComponent(ledIntenso, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		panelIntenso.setLayout(gl_panel_4);
		
		btnIntenso = new JButton("Intenso");
		panelCentral.add(btnIntenso);
		btnIntenso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lavavajillas.ProgramaSeleccionado("INTENSO");
			}
		});
		
		
		JPanel panelbotonesOnArranque = new JPanel();
		frmLavavajillas.getContentPane().add(panelbotonesOnArranque, BorderLayout.CENTER);
		panelbotonesOnArranque.setLayout(new GridLayout(2, 0, 0, 0));
		
		JPanel panelONOFF = new JPanel();
		panelbotonesOnArranque.add(panelONOFF);
		
		
		ledON.setBackground(new Color(169, 169, 169));
		
		btnOnoff = new JButton("ON/OFF");
		//Acción de encender y apagar
		btnOnoff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					lavavajillas.onOff();
			}
		});
		GroupLayout gl_panelONOFF = new GroupLayout(panelONOFF);
		gl_panelONOFF.setHorizontalGroup(
			gl_panelONOFF.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelONOFF.createSequentialGroup()
					.addGap(43)
					.addComponent(ledON, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
					.addGap(30)
					.addComponent(btnOnoff, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(67, Short.MAX_VALUE))
		);
		gl_panelONOFF.setVerticalGroup(
			gl_panelONOFF.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelONOFF.createSequentialGroup()
					.addGap(36)
					.addGroup(gl_panelONOFF.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(btnOnoff, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(ledON, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
					.addContainerGap(39, Short.MAX_VALUE))
		);
		panelONOFF.setLayout(gl_panelONOFF);
		
		JPanel panelArrancar = new JPanel();
		panelbotonesOnArranque.add(panelArrancar);
		
		//Acción de arrancar
		JButton btnArrancar = new JButton("Arrancar");
		btnArrancar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					lavavajillas.arrancar();
			}
		});
		GroupLayout gl_panelArrancar = new GroupLayout(panelArrancar);
		gl_panelArrancar.setHorizontalGroup(
			gl_panelArrancar.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelArrancar.createSequentialGroup()
					.addGap(100)
					.addComponent(btnArrancar, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(107, Short.MAX_VALUE))
		);
		gl_panelArrancar.setVerticalGroup(
			gl_panelArrancar.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelArrancar.createSequentialGroup()
					.addGap(30)
					.addComponent(btnArrancar, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(39, Short.MAX_VALUE))
		);
		panelArrancar.setLayout(gl_panelArrancar);
		
	}
	
}
