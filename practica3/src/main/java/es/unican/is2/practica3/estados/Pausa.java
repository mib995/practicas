package es.unican.is2.practica3.estados;

import es.unican.is2.practica3.Lavavajillas;

/**
 * Clase que representa el estado de pausa del lavavajillas
 * @author Mario
 *
 */
public class Pausa extends LavavajillasState {

	/**
	 * Constructor que crea el estado
	 */
	public Pausa() {}

	@Override
	public void entryAction(Lavavajillas ctx) {
		ctx.setTiempoPausa(System.currentTimeMillis());
	}

	@Override
	public void exitAction(Lavavajillas ctx) {
		ctx.setTiempoPausa(System.currentTimeMillis()-ctx.getTiempoPausa());
	}

	@Override
	public void doAction(Lavavajillas ctx) {}

	@Override
	public void OnOff(Lavavajillas ctx) {
		this.exitAction(ctx);
		Apagado e = LavavajillasState.getApagado();
		ctx.setState(e);
		e.entryAction(ctx);
		e.doAction(ctx);
	}

	@Override
	public void Arrancar(Lavavajillas ctx) {}

	@Override
	public void ProgramaSeleccionado(String nombre, Lavavajillas ctx) {}

	@Override
	public void PuertaAbierta(Lavavajillas ctx) {}

	@Override
	public void PuertaCerrada(Lavavajillas ctx) {
		this.exitAction(ctx);
		Lavando la = LavavajillasState.getLavando();
		ctx.setState(la);
		la.entryAction(ctx);
		la.doAction(ctx);
	}

}
