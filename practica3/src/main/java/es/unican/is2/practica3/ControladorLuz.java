package es.unican.is2.practica3;

import java.awt.Color;

import javax.swing.JButton;

/**
 * Clase que controla el encendido y apagado de las luces
 * @author Mario
 *
 */
public class ControladorLuz{

	private String nombre;
	private JButton led;
	
	/**
	 * Contructor del controlado de la luz
	 * @param nombre nombre del led ha controlar
	 * @param led led que se quiere controlar
	 */
	public ControladorLuz(String nombre, JButton led) {
		this.nombre=nombre;
		this.led = led;
	}
	
	/**
	 * Método que enciende la luz
	 */
	public void on(){
		led.setBackground(new Color(51,255,51));
	}

	/**
	 * Método que apaga la luz
	 */
	public void off(){
		led.setBackground(new Color(169,169,169));
	}

	/**
	 * Método que retorna el nombre de la luz
	 * @return nombre de la luz
	 */
	public String getNombre() {
		return nombre;
	}
}
