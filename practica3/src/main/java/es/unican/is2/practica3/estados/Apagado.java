package es.unican.is2.practica3.estados;

import es.unican.is2.practica3.Lavavajillas;
import es.unican.is2.practica3.ProgramaLavado;

/**
 * Clase que representa el estado apagado del lavavajillas
 * @author Mario
 *
 */
public class Apagado extends LavavajillasState{

	/**
	 * Constructor que crea el estado
	 */
	public Apagado() {}

	@Override
	public void entryAction(Lavavajillas ctx) {
		ctx.setTiempoFinLavado(0);	
		ctx.apagarLuces();
		ctx.setArrancar(false);
	}

	@Override
	public void exitAction(Lavavajillas ctx) {
		ProgramaLavado p = ctx.buscarPrograma("ECO");
		ctx.setProgramaElegido(p);
		ctx.setTiempoFinLavado(p.getTiempo());
	}

	@Override
	public void doAction(Lavavajillas ctx) {}

	@Override
	public void OnOff(Lavavajillas ctx) {
		this.exitAction(ctx);
		Encendido e = LavavajillasState.getEncendido();
		ctx.setState(e);
		ctx.encenderLuzPrograma("On/Off");
		ctx.encenderLuzPrograma("ECO");
		e.entryAction(ctx);
		e.doAction(ctx);
	}

	@Override
	public void Arrancar(Lavavajillas ctx) {}

	@Override
	public void ProgramaSeleccionado(String nombre, Lavavajillas ctx) {}

	@Override
	public void PuertaAbierta(Lavavajillas ctx){}

	@Override
	public void PuertaCerrada(Lavavajillas ctx) {}

}
