package es.unican.is2.practica3.estados;

import es.unican.is2.practica3.Lavavajillas;

/**
 * Clase abstracta que define el estado del lavavajilas
 * @author Mario
 *
 */
public abstract class LavavajillasState {

	//tipos de estados del lavavajillas
	private static Apagado apagado = new Apagado();
	private static Encendido encendido = new Encendido();
	private static Lavando lavando = new Lavando();
	private static Pausa pausa = new Pausa();
	
	/**
	 * Método que inicia el estado del lavavajillas en apagado
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 * @return objeto LavavajillasState que indica el estado del lavavajillas en apagado
	 */
	public static LavavajillasState init(Lavavajillas ctx){
		apagado.entryAction(ctx);
		return apagado;
	}

	/**
	 * Método que realiza las acciones de entrada a un estado
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void entryAction(Lavavajillas ctx);

	/**
	 * Método que realiza las acciones de salida de un estado
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void exitAction(Lavavajillas ctx);
	
	/**
	 * Método que realiza las acciones de un estado mientras se esté en el
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void doAction(Lavavajillas ctx);
	
	/**
	 * Método que indica la acción ha realizar cuando llega la señal
	 * On/Off
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void OnOff(Lavavajillas ctx);
	
	/**
	 * Método que indica la acción ha realizar cuando llega la señal
	 * Arrancar
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void Arrancar(Lavavajillas ctx);
	
	/**
	 * Método que indica la acción ha realizar cuando llega la señal
	 * ProgramaSeleccionad
	 * @param nombre nombre del programa seleccionado
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void ProgramaSeleccionado(String nombre,Lavavajillas ctx);
	
	/**
	 * Método que indica la acción ha realizar cuando llega la señal
	 * PuertaAbierta
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void PuertaAbierta(Lavavajillas ctx);
	
	/**
	 * Método que indica la acción ha realizar cuando llega la señal
	 * PuertaCerrada
	 * @param ctx objeto lavavajillas al que se le asocia el estado
	 */
	public abstract void PuertaCerrada(Lavavajillas ctx);

	/**
	 * Método que indica si el lavavajillas está apagado o no
	 * @return retorna true si está apagado y false si no lo está 
	 */
	public boolean estaApagado() {
		return this.equals(apagado);
	}
	
	/**
	 * Método que devuelve el objeto del estado apagado
	 * @return devuelve el objeto del estado apagado
	 */
	public static Apagado getApagado() {
		return apagado;
	}
	
	/**
	 * Método que devuelve el objeto del estado encendido
	 * @return devuelve el objeto del estado encendido
	 */
	public static Encendido getEncendido() {
		return encendido;
	}
	
	/**
	 * Método que devuelve el objeto del estado lavando
	 * @return devuelve el objeto del estado lavando
	 */
	public static Lavando getLavando() {
		return lavando;
	}

	/**
	 * Método que devuelve el objeto del estado pausa
	 * @return devuelve el objeto del estado pausa
	 */
	public static Pausa getPausa() {
		return pausa;
	}
}
