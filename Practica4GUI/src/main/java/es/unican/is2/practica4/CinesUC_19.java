package es.unican.is2.practica4;

import java.time.LocalDate;

public class CinesUC_19 {
	
	public static double precioEntradaSocio(LocalDate fechaUltimaVisita, LocalDate fecha, boolean vip, int puntos) throws DatoIncorrectoException {
		if(fechaUltimaVisita.compareTo(fecha)>0) {
			throw new DatoIncorrectoException(null);
		}
		
		if(fecha.compareTo(LocalDate.now())<0) {
			throw new DatoIncorrectoException(null);
		}
		
		if(puntos<0) {
			throw new DatoIncorrectoException(null);
		}
		
		if(puntos>1000) {
			return 0;
		}
		
		double precio = 0;
		switch (fecha.getDayOfWeek().getValue()) {
		case 3:
			precio = 5;
			break;
		case 6:
		case 7:
			precio = 8;
			break;
		default:
			precio = 7;
		}
		
		if(vip) {
			precio += 1.2;
		}
		
		if(fechaUltimaVisita.compareTo(fecha.minusDays(15))>0) {
			precio *= 0.75;
		}
		
		return precio;
	}

}
