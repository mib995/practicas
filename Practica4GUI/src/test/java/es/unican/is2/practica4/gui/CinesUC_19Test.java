package es.unican.is2.practica4.gui;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;

import es.unican.is2.practica4.CinesUC_19;
import es.unican.is2.practica4.DatoIncorrectoException;

public class CinesUC_19Test {
	
	private static LocalDate hoy;
	
	@BeforeClass
	public static void setup() {
		hoy = LocalDate.now();
	}
	
	@Test
	public void testPrecioEntrada() {
		try {
			//v�lidos
			assertEquals(CinesUC_19.precioEntradaSocio( obtenerDiaSemana(1).minusDays(3) , obtenerDiaSemana(1) , true, 0), 6.15, 0.01);
			assertEquals(CinesUC_19.precioEntradaSocio(obtenerDiaSemana(0).minusDays(1) , obtenerDiaSemana(0) , false, 500),5.25, 0.01);
			assertEquals(CinesUC_19.precioEntradaSocio(obtenerDiaSemana(2).minusDays(15) , obtenerDiaSemana(2) , false, 1000),5, 0.1);
			assertEquals(CinesUC_19.precioEntradaSocio(obtenerDiaSemana(4).minusDays(14) , obtenerDiaSemana(4) , false, 1001),0, 0.1);
			assertEquals(CinesUC_19.precioEntradaSocio(obtenerDiaSemana(5).minusDays(20) , obtenerDiaSemana(5) , false, 1100),0, 0.1);
			assertEquals(CinesUC_19.precioEntradaSocio(obtenerDiaSemana(6).minusDays(20) , obtenerDiaSemana(6) , false, 1000),8, 0.1);
			assertEquals( CinesUC_19.precioEntradaSocio(obtenerDiaSemana(1) , obtenerDiaSemana(1) , true, 0), 6.15 , 0.01 );
			
			//no v�lidos
			
			try {
			     CinesUC_19.precioEntradaSocio(obtenerDiaSemana(1).plusDays(3) , obtenerDiaSemana(1) , true, 0);
			     fail();
			}catch (DatoIncorrectoException e) {}
			
			try {
			     CinesUC_19.precioEntradaSocio(LocalDate.now().minusDays(3) , LocalDate.now().minusDays(1) , true, 0);
			     fail();
			}catch (DatoIncorrectoException e) {}
			
			try {
			     CinesUC_19.precioEntradaSocio(LocalDate.now().minusDays(5) , LocalDate.now().minusDays(3) , true, 0);
			     fail();
			}catch (DatoIncorrectoException e) {}
			
			try {
			     CinesUC_19.precioEntradaSocio(LocalDate.now().minusDays(3) , LocalDate.now() , true, -1);
			     fail();
			}catch (DatoIncorrectoException e) {}
			
			try {
			     CinesUC_19.precioEntradaSocio(LocalDate.now().minusDays(3) , LocalDate.now() , true, -50);
			     fail();
			}catch (DatoIncorrectoException e) {}
			
			
		} catch (DatoIncorrectoException e) {
			e.printStackTrace();
			fail();
		}
	}

	public LocalDate obtenerDiaSemana(int offset) {
		int dia = hoy.getDayOfWeek().getValue() -1;
		dia = (offset - dia + 7) % 7;
		return LocalDate.now().plusDays(dia);
	}

}