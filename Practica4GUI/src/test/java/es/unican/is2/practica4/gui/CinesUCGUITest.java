package es.unican.is2.practica4.gui;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CinesUCGUITest {

	private FrameFixture demo;
	private static LocalDate hoy = LocalDate.now();

	@Before
	public void setUp() {
		CinesUCGUI sut = new CinesUCGUI();
		demo = new FrameFixture(sut);
		sut.setVisible(true);
	}

	@After
	public void tearDown() {
		demo.cleanUp();
	}

	@Test
	public void test() {;
		demo.textBox("txtFechaUltimaVisita").setText(LocalDate.now().with(
				DayOfWeek.MONDAY).minusDays(21).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
	
		demo.textBox("txtPuntos").setText("50");
		demo.button("btnCalcular").click();
	
		switch(LocalDate.now().getDayOfWeek().getValue()) {
		case 1:
		case 2:
		case 4:
		case 5:
			demo.textBox("txtPrecio").requireText("7.0");
			break;
		case 3:
			demo.textBox("txtPrecio").requireText("5.0");
			break;
		case 6:
		case 7:
			demo.textBox("txtPrecio").requireText("8.0");
			break;
		}
	
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	
		demo.textBox("txtFechaUltimaVisita").setText(LocalDate.now().with(
				DayOfWeek.MONDAY).minusDays(21).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
	
		demo.textBox("txtPuntos").setText("10000");
		demo.button("btnCalcular").click();
	
		demo.textBox("txtPrecio").requireText("0.0");
	
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		demo.textBox("txtFechaUltimaVisita").setText(LocalDate.now().with(
				DayOfWeek.MONDAY).minusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
	
		demo.textBox("txtPuntos").setText("100");
		demo.button("btnCalcular").click();
	
		switch(LocalDate.now().getDayOfWeek().getValue()) {
		case 1:
		case 2:
		case 4:
		case 5:
			demo.textBox("txtPrecio").requireText("5.25");
			break;
		case 3:
			demo.textBox("txtPrecio").requireText("3.75");
			break;
		case 6:
		case 7:
			demo.textBox("txtPrecio").requireText("6.0");
			break;
		}
	}

	public LocalDate obtenerDiaSemana(int offset) {
		int dia = hoy.getDayOfWeek().getValue() -1;
		dia = (offset - dia + 7) % 7;
		return LocalDate.now().minusDays(dia);
	}

}
