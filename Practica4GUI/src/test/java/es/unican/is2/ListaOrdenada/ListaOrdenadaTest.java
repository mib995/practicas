package es.unican.is2.ListaOrdenada;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import es.unican.is2.containers.IListaOrdenada;

public class ListaOrdenadaTest {

	public ListaOrdenadaTest() {}
	
	@Test
	public void testAdd() {
		IListaOrdenada<Integer> lista = new ListaOrdenada<Integer>();
		lista.add(0);
		assertTrue(lista.get(0)==0);
		assertTrue(lista.size()==1);
		lista.add(2);
		lista.add(4);
		lista.add(-1);
		assertTrue(lista.get(0)==-1);
		assertTrue(lista.get(2)==2);
		assertTrue(lista.get(1)==0);
		assertTrue(lista.get(3)==4);
		assertTrue(lista.size()==4);
	}
	
	
	@Test
	public void testRemove() {
		IListaOrdenada<Integer> lista = new ListaOrdenada<Integer>();
		try {
			lista.remove(0);
			fail("Fallo al eliminar un elemento de la lista vacia");
		}catch (IndexOutOfBoundsException e) {}
	
		try {
			lista.get(-1);
			fail("Fallo al eliminar un elemento con indice negativo");
		}catch (IndexOutOfBoundsException e) {}
	
		lista.add(1);
		assertTrue(lista.remove(0)==1);
		assertTrue(lista.size()==0);
		lista.add(1);
		lista.add(2);
		lista.add(4);
		lista.add(-1);
		assertTrue(lista.remove(0)==-1);
		assertTrue(lista.size()==3);
		assertTrue(lista.remove(2)==4);
		assertTrue(lista.size()==2);
		
	}
	
	@Test
	public void testSize() {
		IListaOrdenada<Integer> lista = new ListaOrdenada<Integer>();
		assertTrue(lista.size()==0);
		lista.add(1);
		assertTrue(lista.size()==1);
		lista.remove(0);
		assertTrue(lista.size()==0);
		lista.add(1);
		lista.add(2);
		lista.add(4);
		assertTrue(lista.size()==3);
	}
	
	@Test
	public void testClear() {
		IListaOrdenada<Integer> lista = new ListaOrdenada<Integer>();
		lista.add(1);
		lista.clear();
		assertTrue(lista.size()==0);
		lista.clear();
		assertTrue(lista.size()==0);
		lista.add(1);
		lista.add(2);
		lista.add(4);
		lista.clear();
		assertTrue(lista.size()==0);
	}

	@Test
	public void testGet() {
		IListaOrdenada<Integer> lista = new ListaOrdenada<Integer>();
		try {
			lista.get(0);
			fail("Fallo al devolver un elemento de la lista vacia");
		}catch (IndexOutOfBoundsException e) {}
		lista.add(1);
		assertTrue(lista.get(0)==1);
		lista.add(2);
		lista.add(3);
		lista.add(-1);
		assertTrue(lista.get(0)==-1);
		assertTrue(lista.get(2)==2);
		try {
			lista.get(-1);
			fail("Fallo al devolver un elemento con indice negativo");
		}catch (IndexOutOfBoundsException e) {}
	}
	
}
